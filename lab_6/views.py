from django.shortcuts import render

response = {}
def index(request):
    response['author'] = "Raka Pramada"
    html = 'lab_6/lab_6.html'
    return render(request, html, response)
