from django.shortcuts import render
from lab_2.views import landing_page_content
from lab_1.views import mhs_name
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

# Create your views here.
respone = {'author': mhs_name}
about_me = ['DotA','belajar','tidur','ngoding','makan','main']

def index(request):
    respone['content'] = landing_page_content
    html = 'lab_4/lab_4.html'
    respone['about_me'] = about_me
    respone['message_form'] = Message_Form
    return render(request, html, respone)

def message_post(request):
    form = Message_Form(request.POST or None)

    _POST_ = request.POST
    if(request.method == 'POST' and form.is_valid()):
        respone['name'] = _POST_['name'] if _POST_['name'] != "" else "Anonymous"
        respone['email'] = _POST_['email'] if _POST_['email'] != "" else "Anonymous"
        respone['message'] = _POST_['message']

        message = Message(
            name=respone['name'],
            email=respone['email'],
            message=respone['message']
        )
        message.save()
        html = 'lab_4/form_result.html'
        return render(request, html, respone)

    else:
        return HttpResponseRedirect('/lab_4')

def message_table(request):
    message = Message.objects.all()
    respone['message'] = message
    html = 'lab_4/table.html'
    return render(request, html, respone)
