1
    1) index dipanggil, mengecek apakah user sudah login, kalau belum akan dialihkan ke halaman login
    Di dalam halaman login ketika disubmit akan memanggil fungsi auth_login di custom_auth yang akan
    mengecek validitas username dan password dengan memanggil csui_helper. Apabila data sudah valid
    maka akan dipanggil fungsi index dengan mengubah flag login menjadi true. Setelah itu akan masuk
    ke profile. Untuk logout, tinggal masuk ke auth_logout dan menghapus session yang ada.
    2) Semua data2 mengenai user seperti nama dan kode identitas disimpan di dalam models. Selain itu,
    semua daftar watch later juga disimpan di dalam database yang memliki foreign key yang direference
    ke objek Pengguna, sehingga data yang disimpan sudah relevan dan tidak tertukar

2
    1) Set API key untuk bisa mengakses OMDB dengan mendaftarkannya di website. Setelah itu fungsi yang
    digunakan adalah fungsi search_movie untuk mengakses API dan mendapatkan film berdasarkan search
    input yang diketik
    2) Di dalam ombdb API diambil hanya 30 film saja yang paling banyak. Untuk bagian html, di file
    list.html terdapat method untuk DataTable yang berisi AJAX untuk mengambil data
    3) Fungsi di dalam omdb_api yang digunakan adalah search_movie yang menerima parameter judul film
    dan tahun. Dari fungsi tersebut akan memanggil API omdb dan mendapatkan kembalian berupa json berisi
    data-data film yang memiliki judul hampir sama seperti yang disearch tadi.
    4) Sudah dijelaskan di atas

3
    1) Diimplementasikan ketika kondisi user belum login dan menambahkan film ke watch later. Ada di views
    di bagian add_watch_later. Setelah itu, dicek apakah film tersebut sudah ada di dalam session. Kalau
    sudah ada berarti terdeteksi bahwa user menginput secara manual ke url.
    2) Di dalam views, apabila user sudah login maka data watch later akan dimasukkan ke dalam database.
    3) Menyimpan data film di dalam session, apabila id film tersebut sudah ada, maka tidak akan disimpan
    lagi. Terdapat di bagian utils.py. Dalam setiap request pasti terdapat session yang menyimpan data-data
    sementara client. Session tersebut yang digunakan untuk menyimpan data watch later ketika user belum
    login.
    4) Create user dulu kalau belum pernah login. Setelah itu, ketika user klik add watch later, maka akan
    otomatis menyimpan data film ke database. Fungsinya terdapat di utils.py. Fungsi yang ada di utils ini
    akan dipanggil apabila user sudah login, yang dicek di views.

4
    1) Sudah dari lab 1
    2) Mengecek bentuk status code yang dikembalikan apabila memasukkan url tertentu. Selain itu, mengecek
    jenis-jenis message yang ditampilkan, jika memang di fungsi tersebut diimplementasikan. Yang perlu dicek
    juga response data film yang dikembalikan. Berbagai kondisi seperti di omdb_api juga perlu dites.